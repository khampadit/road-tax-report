<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>

<br><br>
<div class="container">	
	<h1>Road Tax Report</h1>
	<br><br>
</div>

<div class="container">
     <form>
         <section>
             <div class="panel panel-footer" >
                 <table class="table table-bordered">
                     <thead>
                         <tr align="center">
                             <th>ID</th>
                             <th>Serial Number</th>
                             <th>License Plate</th>
                             <th>Province</th>
                             <th>Purpose</th>
                             <th>Vehicle Type</th>
                             <th>Engine CC</th>
                             <!-- <th>Weight</th> -->
                             <th>Fiscal Year</th>
                             <!-- <th>Road Fee</th> -->
                             <th>Payment Date</th>
                             <!-- <th>Receipt</th> -->
                         </tr>
                     </thead>
                     <tbody>
         @foreach ($road_taxes as $road_tax)
         <tr>
         <td align="center">{{ $road_tax->id }}</td>
         <td align="center">{{ $road_tax->serial_number }}</td>   
         <td align="center">{{ $road_tax->license_plate_number }}</td>
         <td align="center">{{ $road_tax->province }}</td>
         <td align="center">{{ $road_tax->purpose }}</td>   
         <td align="center">{{ $road_tax->vehical_type }}</td>
         <td align="center">{{ $road_tax->engine_cc }}</td>
         <td align="center">{{ $road_tax->fiscal_year }}</td>
         <td align="center">{{ $road_tax->payment_date }}</td>   
         <!-- <td align="center">
            <a href="" class="btn btn-success"> ດັດແກ້ </a>
            <a href="" class="btn btn-danger"> ລົບ </a>
         </td> -->
         </tr>
         @endforeach
                     </tbody>
                 </table>
             </div>
         </section>
     </form>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>